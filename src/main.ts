import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      // forbidNonWhitelisted: true, // will not accept any non-whitelisted params
      transform: true, // will transform the data into a plain object
    }),
  );
  await app.listen(3000, () =>
    console.log('Server is running on http://localhost:3000'),
  );
}
bootstrap();
